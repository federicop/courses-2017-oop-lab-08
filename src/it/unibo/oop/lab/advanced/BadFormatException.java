package it.unibo.oop.lab.advanced;

/**
 * Usually thrown during parsing of bad formatted input.
 */
public class BadFormatException extends RuntimeException {
    private static final long serialVersionUID = -5218895230296557201L;

}
