package it.unibo.oop.lab.advanced;

/**
 * Models the app configuration.
 */
public interface Config {
    /**
     *
     * @return The minimum value for the number to be guessed.
     */
    int getMinNumberValue();

    /**
     *
     * @return The maximum value for the number to be guessed.
     */
    int getMaxNumberValue();

    /**
     *
     * @return The minimum guessing attempts.
     */
    int getMaxAttempts();
}
