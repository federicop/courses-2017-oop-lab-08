package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;

/**
 * Class to load {@link Config}.
 */
public final class ConfigLoader {
    private ConfigLoader() {

    }

    /**
     * Load {@link Config} from a YML-like stream. It currently only supports
     * key-value pairs separated by a colon.
     *
     * @param source
     *            The {@link InputStream} to load the {@link Config} from.
     * @return {@link Config}
     * @throws IOException
     *             If an error occurs during the reading of the {@link InputStream}.
     * @throws BadFormatException
     *             If the configuration is badly formatted.
     *
     */
    public static Config load(final InputStream source) throws IOException, BadFormatException {
        final Map<String, String> confs = new HashMap<>();
        Objects.requireNonNull(source);

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(source));) {
            String line;
            while ((line = reader.readLine()) != null) {
                final String[] fields = line.split(":");
                if (fields.length >= 2) {
                    confs.put(fields[0], fields[1]);
                }
            }
        }

        try {
            final int min = Integer.parseInt(getValue(confs, "minimum"));
            final int max = Integer.parseInt(getValue(confs, "maximum"));
            final int attempts = Integer.parseUnsignedInt(getValue(confs, "attempts"));
            return new LoadedConfig(min, max, attempts);
        } catch (final NoSuchElementException e1) {
            throw new BadFormatException();
        } catch (final NumberFormatException e2) {
            throw new BadFormatException();
        }
    }

    private static String getValue(final Map<String, String> map, final String key) throws NoSuchElementException {
        final String val = map.get(key);
        if (val == null) {
            throw new NoSuchElementException(key);
        }
        return val.trim();
    }

    private static final class LoadedConfig implements Config {
        private final int min, max, attempts;

        private LoadedConfig(final int min, final int max, final int attempts) {
            this.min = min;
            this.max = max;
            this.attempts = attempts;
        }

        @Override
        public int getMinNumberValue() {
            return min;
        }

        @Override
        public int getMaxNumberValue() {
            return max;
        }

        @Override
        public int getMaxAttempts() {
            return attempts;
        }

    }
}
