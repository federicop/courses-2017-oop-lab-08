package it.unibo.oop.lab.advanced;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import it.unibo.oop.lab.advanced.views.ConsoleView;
import it.unibo.oop.lab.advanced.views.DrawNumberImpl;
import it.unibo.oop.lab.advanced.views.DrawNumberView;
import it.unibo.oop.lab.advanced.views.DrawNumberViewImpl;
import it.unibo.oop.lab.advanced.views.FileLoggerView;

/**
 */
public class DrawNumberApp implements DrawNumberViewObserver {

    private static final String CONFIG_FILE = "/config.yml";
    private static final File DEFAULT_LOG_FILE = new File("log.txt");
    private final Config config;
    private final DrawNumber model;
    private final Collection<DrawNumberView> views = new ArrayList<>();

    /**
     * TODO: handle the exceptions.
     *
     * @throws IOException
     *             If an error occurs during the loading of the configuration file.
     */
    public DrawNumberApp() throws IOException {
        config = ConfigLoader.load(DrawNumberApp.class.getResourceAsStream(CONFIG_FILE));

        this.model = new DrawNumberImpl(config.getMinNumberValue(), config.getMaxNumberValue(),
                config.getMaxAttempts());
        registerView(new DrawNumberViewImpl());
        registerView(new FileLoggerView(DEFAULT_LOG_FILE));
        registerView(new ConsoleView());
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            views.forEach(v -> v.result(result));
        } catch (final IllegalArgumentException e) {
            views.forEach(DrawNumberView::numberIncorrect);
        } catch (final AttemptsLimitReachedException e) {
            views.forEach(DrawNumberView::limitsReached);
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        views.forEach(DrawNumberView::stop);
    }

    private void registerView(final DrawNumberView view) {
        views.add(view);
        view.setObserver(this);
        view.start();
    }

    /**
     * @param args
     *            ignored
     * @throws IOException
     *             TODO: handle it.
     */
    public static void main(final String... args) throws IOException {
        new DrawNumberApp();
    }

}
