package it.unibo.oop.lab.advanced.views;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Logs the game state to a {@link File}.
 */
public class FileLoggerView extends OutputView {
    /**
     *
     * @param file
     *            The {@link File} to use.
     * @throws FileNotFoundException
     *             If the {@link File} is not found.
     */
    public FileLoggerView(final File file) throws FileNotFoundException {
        super(new FileOutputStream(file));
    }

}
