package it.unibo.oop.lab.advanced.views;

/**
 * Prints the game state to the CLI.
 */
public class ConsoleView extends OutputView {

    /**
     * .
     */
    public ConsoleView() {
        super(System.out);
    }

}
