package it.unibo.oop.lab.advanced.views;

import java.io.OutputStream;
import java.io.PrintWriter;

import it.unibo.oop.lab.advanced.DrawNumberViewObserver;
import it.unibo.oop.lab.advanced.DrawResult;

/**
 * Represents the game state as text output through an {@link OutputStream}.
 */
public class OutputView implements DrawNumberView {

    private final PrintWriter output;

    /**
     *
     * @param os
     *            The {@link OutputStream} to use.
     */
    public OutputView(final OutputStream os) {
        output = new PrintWriter(os, true); // true means "enable auto-flush"
    }

    @Override
    public void setObserver(final DrawNumberViewObserver observer) {
        // no need for it.
    }

    @Override
    public void start() {
        output.println("Game started");
    }

    @Override
    public void numberIncorrect() {
        output.println("Incorrect number!");
    }

    @Override
    public void result(final DrawResult res) {
        switch (res) {
            case YOU_WON:
                output.println(res.getDescription() + ": a new game starts!");
                break;
            default:
                output.println(res.getDescription());
        }
    }

    @Override
    public void limitsReached() {
        output.println("You lost: a new game starts!");
    }

    @Override
    public void displayError(final String message) {
        output.println("ERROR: " + message);
    }

    @Override
    public void stop() {
        output.println("Quitting... Bye bye!");
        output.flush(); // redundant but call it anyway to be future-proof
        output.close();
    }

}
