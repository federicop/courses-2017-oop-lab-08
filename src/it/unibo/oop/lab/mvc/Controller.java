package it.unibo.oop.lab.mvc;

import java.util.List;

/**
 *
 */
public interface Controller {

    /*
     * This interface must model a simple controller responsible of I/O access.
     * It considers only the standard output, and it is able to print on it.
     *
     * Write the interface and implement it in a class class in such a way that
     * it includes:
     *
     * 1) A method for setting the next string to print. Null values are not
     * acceptable, and an exception should be produced
     *
     * 2) A method for getting the next string to print
     *
     * 3) A method for getting the history of the printed strings (in form of a
     * List of Strings)
     *
     * 4) A method that prints the current string. If the current string is
     * unset, an IllegalStateException should be thrown
     *
     */

    /**
     * A method for setting the next string to print.
     * @param string - The next {@link String} to print
     * @throws IllegalArgumentException if the supplied string is null
     */
    void setNextString(String string) throws IllegalArgumentException;

    /**
     * A method for getting the next string to print.
     * @return {@link String} The next string to print.
     */
    String getNextString();

    /**
     *
     * @return List<String> - the history of printed strings
     */
    List<String> getHistory();

    /**
     * Prints the current string.
     * @throws IllegalStateException If the current string is unset
     */
    void printString() throws IllegalStateException;

}
